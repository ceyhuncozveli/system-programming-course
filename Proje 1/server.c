#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <signal.h>
#include <ctype.h>
#include "restart.h"

#define MAX_PATH_LENGHT 256
#define MAX_LEN   100
#define NOT_FOUND -1

#define FIFO_NAME "fifoOfServer"

void sigExit();
void directoryTraveller(const char *dirpath,const char *fifoName,const int fdp);
int isdirectory(char *path);
char *deleteOnce(char *source, int index, int n);
int   pos(const char *source, const char *to_find);
char *deleteAll(char *source);

int main(void)
{
    int i=0;
    int child=5;
    int fdp;
    int controlRead;
    int controlChild;
    int controlParent;
    int controlWrite;
    char pathName[MAX_PATH_LENGHT];
    int pointerFile;
    int fds[2]; /* pipe haberlesmesi icin */
    char dene[MAX_PATH_LENGHT];
    char specialFifoName[MAX_PATH_LENGHT];

    char testStr [100] = {[0 ... 99] = '\0'};

    signal(SIGINT,sigExit);

    printf("ppid: %d\n", getpid());

    /* Clientlarin isteklerini yazacaklari serverin okuma yapacagi fifo olusturuluyor */
    if (mkfifo(FIFO_NAME, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH) < 0)
    {
        perror("mkfifo");
        exit(EXIT_FAILURE);
    }

        if(pipe(fds) < 0)
        {
            perror("pipe");
            exit(EXIT_FAILURE);
        }

        /* Serverin istekleri okuyacagi fifo okuma modunda aciliyor */
        if( (fdp = open("fifoOfServer",O_RDONLY)) < 0 )
        {
            perror("open of fifoOfServer");
            exit(EXIT_FAILURE);
        }

    while(1)
    {

             /* Server burada Client'in istegini(directory path'ini) okuyor ve stringe atiyor. */
            controlRead = read(fdp,pathName,sizeof(pathName));
            if(controlRead < 0)
            {
                sprintf(testStr, "pid: %d -- read of Parent", getpid());
                perror(testStr);
                exit(EXIT_FAILURE);
            } else if(controlRead == 0) {
                /*printf("pid: %d -- Waiting child...\n", getpid());*/
                continue;
            } else{
                printf("%s\n",pathName);
                printf("Ben ata islem : %s\n",pathName);

                child = fork();
            }

        if(child < 0)
        {
            perror("fork");
            exit(EXIT_FAILURE);
        }




        if(child == 0) /* COCUK PROSES  */ /* pipe'tan okur */
        {
            close(fds[1]);
            if( (controlRead = read(fds[0],dene,sizeof(dene)) ) < 0)
            {
                perror("read of Pipe at Child");
                exit(EXIT_FAILURE);
            }
            close(fds[0]);

            strcpy(specialFifoName,dene);
            deleteAll(specialFifoName);
            printf("pid: %d -- Ben cocuk islem : %s\n",getpid(), specialFifoName);

            /* Server directoryleri fifoya yaziyor*/
             if( (pointerFile = open(specialFifoName,O_WRONLY)) < 0 )
            {
                perror("open of Fifo at Child");
                exit(EXIT_FAILURE);
            }

            directoryTraveller(dene,specialFifoName,pointerFile);
            close(pointerFile);
            return 0;
            wait(NULL);
        }
        else    /* BABA PROSES */ /* pipe'a yazar */
        {
            /* Server burada Client'in istegini(directory path'ini) okuyor ve stringe atiyor. */
            /*if( (controlRead = read(fdp,pathName,sizeof(pathName)) ) <= 0)
            {
                sprintf(testStr, "pid: %d -- read of Parent", getpid());
                perror(testStr);
                exit(EXIT_FAILURE);
            }
            printf("%s\n",pathName);
            printf("Ben ata islem : %s\n",pathName);*/

            /*close(fds[0]);*/
            if( (controlWrite = write(fds[1],&pathName,sizeof(pathName))) <= 0)
            {
                perror("write of Parent");
                exit(EXIT_FAILURE);
            }
            printf("Atanin pipe'a yazdigi :%s\n",pathName);
            /*close(fds[1]);*/

            /*close(fdp);*/
        }




    }


    close(fdp);

    return 0;
}




char *
deleteOnce(char *source,  /* input/output - string from which to delete part */
       int   index,   /* input - index of first char to delete           */
       int   n)       /* input - number of chars to delete               */
{
      char rest_str[MAX_LEN];  /* copy of source substring following
                                  characters to delete */

      /*  If there are no characters in source following portion to
          delete, delete rest of string */
      if (strlen(source) <= index + n) {
            source[index] = '\0';

      /*  Otherwise, copy the portion following the portion to delete
          and place it in source beginning at the index position        */
      } else {
            strcpy(rest_str, &source[index + n]);
            strcpy(&source[index], rest_str);
      }

      return (source);
}



int pos(const char *source,const char *to_find)

{
      int  i = 0, find_len, found = 0, position;
      char substring[MAX_LEN];

      find_len = strlen(to_find);

      while (!found  &&  i <= strlen(source) - find_len) {
          strncpy(substring, &source[i], find_len);
          substring[find_len] = '\0';

          if (strcmp(substring, to_find) == 0)
                found = 1;
          else
                ++i;
      }

      if (found)
            position = i;
      else
            position = NOT_FOUND;

      return (position);
}

char* deleteAll(char *source)
{

    while(pos(source,"/") != -1)
    {
        deleteOnce(source,pos(source,"/"),1);
    }
    return source;
}

void sigExit()
{
    printf("\nCikis Yapiliyor!\n");
    unlink(FIFO_NAME);
    exit(0);
}

int isdirectory(char *path)
{
    struct stat statbuf;
    if (stat(path, &statbuf) == -1)
    return 0;
    else return S_ISDIR(statbuf.st_mode);
}

void directoryTraveller(const char *dirpath,const char *fifoName,const int fdp)
{
    DIR *dir;
    struct dirent *ent;
    char path[MAX_PATH_LENGHT];
    int i=0;
    int controlWrite;

    if((dir = opendir(dirpath)) == NULL)
    {
        perror("opendir");
        return;
    }

    while((ent = readdir(dir) ) != NULL)
    {

        if(!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
        continue;

        snprintf(path, MAX_PATH_LENGHT, "%s/%s", dirpath, ent->d_name);
        /*printf("%s\n",path);*/


        if( (controlWrite = write(fdp,&path,sizeof(path))) <= 0)
        {
            perror("write");
            exit(EXIT_FAILURE);
        }



        if(isdirectory(path) != 0)
        {
            directoryTraveller(path,fifoName,fdp);
        }

    }


    closedir(dir);

}





