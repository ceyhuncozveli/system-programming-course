/*
	Author: Ceyhun ÇÖZVELİOĞLU
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <ctype.h>
#include <limits.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>

#define BLKSIZE PIPE_BUF
#define MILLION 1000000L
#define D_MILLION 1000000.0
#define MAX_PATH_LENGHT 256
#define MAX_LEN   10000
#define NOT_FOUND -1

void sigExit();
int usage();
int options();
char getch(void);
char *deleteOnce(char *source, int index, int n);
int pos(const char *source, const char *to_find);
char *insert(char *source, const char *to_insert, int index);
char *deleteAll(char *source);
int listAllDir(FILE *input);
char *insertAp(char *source,char *to_find);
int listFindDir(FILE *input,char *to_find);
int backupFileDir(int listp,const char *fileName);
int myMore(FILE *input,int satirSayisi);

int main(int argc,char *argv[])
{

    int fdp;
    int listp;
    int controlWrite;
    pid_t child;
    char pathName[MAX_PATH_LENGHT];
    char choice;
    char specialFifoName[MAX_PATH_LENGHT];
    char directoryNames[MAX_PATH_LENGHT];
    char wordOfFind[MAX_LEN];
    char backUpFileName[MAX_PATH_LENGHT];
    int controlRead = 5;
    char karakter;
    int numberOfLines=0;
    FILE *list;
    FILE *find;
    FILE *more;

    signal(SIGINT,sigExit);

    strcpy(pathName,argv[1]);
    strcpy(specialFifoName,argv[1]);
    deleteAll(specialFifoName);

    strcpy(backUpFileName,specialFifoName);
    insert(backUpFileName,".txt",strlen(backUpFileName));
    printf("backupFileName: %s\n",backUpFileName);

    printf("%s\n",pathName);


    if(argc != 2)
    {
        printf("Hatali Baslangic Yaptiniz!\n");
        usage();
        exit(EXIT_FAILURE);
    }

    /* Server'in directoryleri yazdirip Client'in okuyacagi fifomuzu olusturuyoruz */
    if (mkfifo(specialFifoName, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH) < 0)
    {
        perror("mkfifo Ozel");
        exit(EXIT_FAILURE);
    }


    if( (fdp = open("fifoOfServer",O_WRONLY)) <= 0 )
    {
        perror("open");
        exit(EXIT_FAILURE);
    }



    child = fork();

    if(child < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }


    if(child > 0)   /* ATA ISLEM  */
    {
        printf("Atada\n");
        /* FIFO'ya Server'a verilecek directory path'lerinin yazilmasi*/
        if( (controlWrite = write(fdp,&pathName,sizeof(pathName))) <= 0)
        {
            perror("write");
            exit(EXIT_FAILURE);
        }
        printf("Beni yazdirdi\n");
        close(fdp);

        wait(&child);


    }
    else    /* COCUK ISLEM */
    {
        /* Serverin yazdigi directory yollarinin bulundugu FIFO aciliyor. */
        if( (listp = open(specialFifoName,O_RDONLY)) < 0 )
        {
            perror("open");
            exit(EXIT_FAILURE);
        }
        /* FIFO dosyasinin icerigi TXT dosyasina kopyalanip yedek aliniyor. */
        backupFileDir(listp,backUpFileName);

        do{
            options();
            choice = getch();
            printf("%d\n",choice);

            if( (choice == 'q') || (choice == 'Q'))
            {
                break;
            }

            switch(choice)
            {
                case 'L':
                case 'l':
                        printf("l'ye girdi\n");
                        if( (list = fopen(backUpFileName,"r")) == NULL)
                        {
                            printf("Backup File Can Not Open!\n");
                            exit(0);
                        }
                        listAllDir(list);
                        fclose(list);
                        break;

                case 'F':
                case 'f':
                        printf("Word > ");
                        gets(wordOfFind);
                        if( (find = fopen(backUpFileName,"r")) == NULL )
                        {
                            printf("Backup File Can Not Open!\n");
                            exit(0);
                        }
                        listFindDir(find,wordOfFind);
                        fclose(find);
                        break;

                case 'M':
                case 'm':
                        if( (more = fopen(backUpFileName,"r")) == NULL)
                        {
                            printf("Backup File Can Not Open!\n");
                            exit(0);
                        }
                        printf("Please enter the number of line\n");
                        printf("0 for default\n");
                        printf("> ");
                        scanf("%d",&numberOfLines);
                        if(numberOfLines == 0)
                            numberOfLines = 24;
                        myMore(more,numberOfLines);

                        karakter = getch();

                        do{

                            if(karakter == ' ')
                            {
                                myMore(more,numberOfLines);
                            }
                            else if(karakter == '\n')
                            {
                                myMore(more,1);
                            }
                            karakter = getch();

                        }while(more != NULL);


                        break;
                default:
                        printf("\nWRONG INPUT VALUE!\n");
                        break;
            }


        }while( (choice != 'Q') || (choice != 'q') );

    }

    /* Backup dosyasi ve directorylerin bulundugu FIFO butun islemler bitince silinir. */
    unlink(specialFifoName);
    unlink(backUpFileName);
    return 0;
}

void sigExit()
{
    printf("Cikis Yapildi\n");
    exit(0);
}

int usage()
{
    printf("Usage : ./client /directoryPath\n");
}

char getch(void)
{
  char ch;
  struct termios oldt;
  struct termios newt;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

  return ch;
}


int options()
{
    printf("> List (L)\n");
    printf("> Find (F)\n");
    printf("> More (M)\n");
    printf("> Time (T)\n");
    printf("> Kill (K)\n");
    printf("> Quit (Q)\n");
}

char *
deleteOnce(char *source,int   index,int   n)
{
      char rest_str[MAX_LEN];

      if (strlen(source) <= index + n) {
            source[index] = '\0';

      } else {
            strcpy(rest_str, &source[index + n]);
            strcpy(&source[index], rest_str);
      }

      return (source);
}



int pos(const char *source,const char *to_find)

{
      int  i = 0, find_len, found = 0, position;
      char substring[MAX_LEN];

      find_len = strlen(to_find);

      while (!found  &&  i <= strlen(source) - find_len) {
          strncpy(substring, &source[i], find_len);
          substring[find_len] = '\0';

          if (strcmp(substring, to_find) == 0)
                found = 1;
          else
                ++i;
      }

      if (found)
            position = i;
      else
            position = NOT_FOUND;

      return (position);
}

char *deleteAll(char *source)
{

    while(pos(source,"/") != -1)
    {
        deleteOnce(source,pos(source,"/"),1);
    }
    return source;
}

/* txt dosyasindan directory isimlerini okuyup ekrana yazdiracak. */
int listAllDir(FILE *input)
{
    int controlRead = 5;
    char directoryNames[MAX_PATH_LENGHT];
    char temp[MAX_PATH_LENGHT] = {"BU FONKSIYON BIR HARIKA"};
    printf("fonksiyona girdi\n");

    while(controlRead != NULL)
    {

        controlRead = fgets(directoryNames,MAX_PATH_LENGHT,input);
        if(strcmp(temp,directoryNames) == 0)
        {
            continue;
        }
        printf("%s",directoryNames);
        strcpy(temp,directoryNames);
    }

    return 1;
}


char *insert(char *source,const char *to_insert,int index)
{
      char rest_str[MAX_LEN]; /* copy of rest of source beginning
                                 with source[index] */

      if (strlen(source) <= index) {
            strcat(source, to_insert);
      } else {
            strcpy(rest_str, &source[index]);
            strcpy(&source[index], to_insert);
            strcat(source, rest_str);
      }

      return (source);
}

char *insertAp(char *source,char *to_find)
{
    char temp[MAX_LEN];
    strcpy(temp,source);

    insert(source,"'",pos(source,to_find));
    insert(source,"'", pos(source,to_find) + strlen(to_find) );


    return source;
}

int listFindDir(FILE *input,char *to_find)
{
    int controlRead = 5;
    char directoryNames[MAX_PATH_LENGHT];
    char temp[MAX_PATH_LENGHT] = {"BU FONKSIYON BIR HARIKA"};
    char directoryNamesCopy[MAX_PATH_LENGHT];

    while(controlRead != NULL)
    {

        controlRead = fgets(directoryNames,MAX_PATH_LENGHT,input);
        if(strcmp(temp,directoryNames) == 0)
        {
            continue;
        }
        if(pos(directoryNames,to_find) != NOT_FOUND)
        {
            strcpy(directoryNamesCopy,directoryNames);
            insertAp(directoryNamesCopy,to_find);
            printf("%s",directoryNamesCopy);
        }

        strcpy(temp,directoryNames);
    }

    return 1;
}

/* Fifonun iceriginin yedegini alan fonksiyon */
int backupFileDir(int listp,const char *fileName)
{
    int controlRead = 5;
    char directoryNames[MAX_PATH_LENGHT];
    char temp[MAX_PATH_LENGHT] = {"BU FONKSIYON BIR HARIKA"};
    FILE *filePointer;
    char newFileName[MAX_PATH_LENGHT];


    filePointer = fopen(fileName,"w");
    if(filePointer == NULL)
    {
        printf("Fifo Backup Error!\n");
        exit(0);
    }

    while(controlRead != 0)
    {

        if( (controlRead = read(listp,directoryNames,sizeof(directoryNames)) ) < 0)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
        if(strcmp(temp,directoryNames) == 0)
        {
            continue;
        }
        fprintf(filePointer,"%s\n",directoryNames);
        strcpy(temp,directoryNames);
    }

    fclose(filePointer);

    return 1;
}


int myMore(FILE *input,int satirSayisi)
{
     char liste[MAX_LEN];
     int i=0;
     int sayi=0;
     char *p;


        for(i=0;i<satirSayisi;i++)
        {
            p = fgets(liste,MAX_LEN,input);
            if(p == NULL)
            {
                exit(EXIT_FAILURE);
            }
            printf("%s",liste);

            if(p == NULL)
            {
                exit(EXIT_FAILURE);
            }
        }

    return 1;
}


