/************************************************************/
/*															*/
/*			         Ceyhun COZVELIOGLU						*/
/*						111044042    						*/
/*			        CSE244 Homework3						*/
/*					     myMore								*/
/*															*/
/* Aciklama: Bu program belirtilen bir text dosyasindan	 	*/
/* okunabilirligi artirma adina, kullanicinin girdigi satir */
/* sayisi kadar her space tusuna basildiginda ekrana 		*/
/* dosya icerigini yazdirir, enter'a basildiginda bir satir */
/* bir satir ekrana yazdirir. 								*/
/*															*/
/* Ornek Calistirma Komutu : myMore 2(optional) input.txt   */
/*															*/
/************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

#define MAX_LENGTH 100

/********************************************************************************/
/*																				*/
/* Function Name: getch															*/
/* Parameters	: No Parameters.												*/
/* Description  : Tek bir karakter alip hemen isleme koymak icin kullanilan     */
/*				  fonksiyondur.													*/
/*																				*/
/********************************************************************************/
char getch(void);

/********************************************************************************/
/*																				*/
/* Function Name: usage															*/
/* Parameters	: No Parameters.												*/
/* Description  : Gerekli goruldugu yerde programin nasil kullanilacagina dair  */
/*				  aciklamayi ekrana bastiran fonksiyondur.						*/
/*																				*/
/********************************************************************************/
int usage();

/********************************************************************************/
/*																				*/
/* Function Name: usage															*/
/* Parameters	: 1- FILE *: Icerigi okunacak dosyanin pointer adresidir.		*/
/*				  2- int   : Kac satir kac satir okunacagini bildirmek icindir. */
/* Description  : Gerekli goruldugu yerde programin nasil kullanilacagina dair  */
/*				  aciklamayi ekrana bastiran fonksiyondur.						*/
/*																				*/
/********************************************************************************/
int dosyaOkuVeYaz(FILE *input,int satirSayisi);

int main(int argc,char *argv[])
{
	int satirSayisi = 24;
	char dosyaAdi[MAX_LENGTH];
	FILE *input;
	char karakter;
	
	if( (argc > 3) || (argc < 2) ) 
	{	
		printf("Hatali Giris!\n");
		usage();
		exit(EXIT_FAILURE);
	}
	else if(argc == 3)
	{	
		satirSayisi = *argv[1]-48; /* satirSayisina argv[2]'nin iceriginin atanmasi */
		strcpy(dosyaAdi,argv[2]); /* dosyaAdi'na argv[1]'in iceriginin kopyalanmasi */
	}
	
	if(argc == 2)
	{
		satirSayisi = 24;
		strcpy(dosyaAdi,argv[1]); /* dosyaAdi'na argv[1]'in iceriginin kopyalanmasi */
	}
	
	
	
	
	
	input = fopen(dosyaAdi,"r");
	
	
	if(input == NULL)
	{
		printf("Dosya Acilamadi Lutfen Dosya Adini Kontrol Edin!\n");
		usage();
		exit(EXIT_FAILURE);
	}
	
	printf("::::::::::::::\n");
	printf("%s\n",dosyaAdi);
	printf("::::::::::::::\n");
	
	dosyaOkuVeYaz(input,satirSayisi);
	karakter = getch();
	
	do{
		
		if(karakter == ' ')
			{
				dosyaOkuVeYaz(input,satirSayisi);
			}
				
		else if(karakter == '\n')
			{
		 	 	dosyaOkuVeYaz(input,1);
			}
				
			karakter = getch();
		
	  }while(input != NULL);
	
	fclose(input);
	
	return 0;
}

int usage()
{
	printf("\t\tUSAGE\n");
	printf("./more lineNumber(optional)  inputFle.txt\n");
	
	return 1;
}

int dosyaOkuVeYaz(FILE *input,int satirSayisi)
{
	char liste[MAX_LENGTH];
	 int i=0;
	 int sayi=0;
	 char *p;
	
	
		for(i=0;i<satirSayisi;i++)
		{
			p = fgets(liste,MAX_LENGTH,input);
			if(p == NULL)
			{
				exit(EXIT_FAILURE);
			}
			printf("%s",liste);
			
			if(p == NULL)
			{
				exit(EXIT_FAILURE);
			}
		}

	return 1;


}


char getch(void)
{
  char ch;
  struct termios oldt;
  struct termios newt;
  
  tcgetattr(STDIN_FILENO, &oldt); 
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO); 
  tcsetattr(STDIN_FILENO, TCSANOW, &newt); 
  ch = getchar(); 
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt); 
  
  return ch; 
}


