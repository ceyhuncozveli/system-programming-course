/************************************************************/
/*															*/
/*			         Ceyhun COZVELIOGLU						*/
/*						111044042    						*/
/*			        CSE244 Homework2_Part1					*/
/*					    lsNoFork							*/
/*															*/
/* Aciklama: Bu program "./lsNoFork ~/path" seklinde 	 	*/
/* calistirildiginda path adresindeki butun dosyalari 		*/
/* ekrana basar ve ne kadar directory varsa bunlarin 		*/
/* icine girerek icerdikleri dosyalari,yollarini ve ATA PID	*/
/* degerlerini ekrana basar.								*/
/*															*/
/************************************************************/ 

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>

#define MAX_LENGTH_PATH 2048


/********************************************************************************/
/*																				*/
/* Function Name: isdirectory													*/
/* Parameters	: 1- char *path: Kontrol edilmesi istenen dosyanin adresidir.	*/
/* Description  : Adresi parametre olarak gonderilen dosyanin directory olup    */
/*				  olmadigini kontrol eder eger directory ise 1 degilse 0 return */
/*				  eder.															*/
/*																				*/
/********************************************************************************/
int isdirectory(char *path);

/********************************************************************************/
/*																				*/
/* Function Name: directoryTraveller											*/
/* Parameters	: 1- const char *dirpath: Icindeki dosyalarin ve directorylerin	*/
/*				  gezilip tek tek ekrana basilmasi istenen directory adresidir	*/
/* Description  : Arguman olarak gonderilen adresteki butun directoryleri gezip */
/*				  iceriklerini ve ATA PID degerlerini ekrana bastirir.			*/
/*																				*/
/********************************************************************************/
void directoryTraveller(const char *dirpath);

/********************************************************************************/
/*																				*/
/* Function Name: usage															*/
/* Parameters	: No Parameters.												*/
/* Description  : Gerekli goruldugu yerde programin nasil kullanilacagina dair  */
/*				  aciklamayi ekrana bastiran fonksiyondur.						*/
/*																				*/
/********************************************************************************/
int usage();


int main(int argc,char *argv[])
{
	
	if(argc != 2)
	{
		usage();
		exit(EXIT_FAILURE);
	}
	
	printf("PID\tPATH FOR %s\n",argv[1]);
	printf("-------------------------------\n");
	directoryTraveller(argv[1]);
	printf("\n-------- End Program --------\n");
	
	return 0;

}


int usage()
{
	printf("\t\tUSAGE\n");
	printf("You must start this program such > ");
	printf("./lsNoFork ~/directoryPath\n");

}


int isdirectory(char *path) 
{
	struct stat statbuf;
	if (stat(path, &statbuf) == -1)
	return 0;
	else return S_ISDIR(statbuf.st_mode);
}

void directoryTraveller(const char *dirpath)
{
	DIR *dir;
	struct dirent *ent;
	char path[MAX_LENGTH_PATH];
	int i=0;
	
	
	;
	if((dir = opendir(dirpath)) == NULL)
	{
		perror("opendir");
		return;
	}
	
	
	
	while((ent = readdir(dir) ) != NULL)
	{
	
		if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))	
		continue;
		
		printf("%ld ",(long)getpid());
		snprintf(path, MAX_LENGTH_PATH, "%s/%s", dirpath, ent->d_name);
		printf("%s\n",path);
		
		
		
		if(isdirectory(path) != 0)
		{
			directoryTraveller(path);
		}
	
	}
	
	
	closedir(dir);

}
